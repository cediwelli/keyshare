CC=g++
LIBS=-lX11
WARN=-Wall
INCLUDES=-Iinclude/

SOURCE=$(wildcard src/*.cc)
OBJECTS=$(patsubst src/%.cc,bin/%.o,$(SOURCE))

all: $(OBJECTS)
	$(CC) $(OBJECTS) $(WARN) -o bin/keyshare $(LIBS)

bin/%.o: src/%.cc
	$(CC) -c $(INCLUDES) $< -o $@
