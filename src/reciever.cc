#include "receiver.hh"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <X11/Xlib.h>

void
receiver_run(char * addr, uint16_t port) {

	Display * dpy = XOpenDisplay(NULL);

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in hostaddr;
	hostaddr.sin_family = AF_INET;
	hostaddr.sin_port = htons(port);

	inet_pton(AF_INET, addr, &hostaddr.sin_addr);

	if(connect(sockfd, (struct sockaddr *) &hostaddr, sizeof(hostaddr)) == -1) {
		perror("connect()");
		exit(0);
	}

	size_t bufsize = 3*4 + 1; // 3 Integers + 0db
	char buf[bufsize];

	while(true) {

		if(recv(sockfd, buf, bufsize, MSG_WAITALL) == -1) {
			perror("recv()");
			exit(-1);
		}

		if((unsigned char) (buf[bufsize-1]) != 0xdb) {
			printf("Synchronization error!\n");
			exit(-1);
		}

		for(int i = 0; i < bufsize; ++i) {
			printf("0x%x ", (unsigned char) (buf[i]));
		}
		printf("\n");

		buf[bufsize-1] = 0;

		int type, state, key;
		memcpy(&type, &buf[0], sizeof(unsigned int));
		memcpy(&state, &buf[4], sizeof(unsigned int));
		memcpy(&key, &buf[8], sizeof(unsigned int));

		Window dstWindow;
		int revert;
		XEvent ev;
		XKeyEvent xke;

		XGetInputFocus(dpy, &dstWindow, &revert);

		xke.display      = dpy;
		xke.window       = dstWindow;
		xke.root         = DefaultRootWindow(dpy);
		xke.subwindow    = None;
		xke.time         = CurrentTime;
		xke.x            = 1;
		xke.y            = 1;
		xke.x_root       = 1;
		xke.y_root       = 1;
		xke.same_screen  = True;
		xke.send_event   = True;

		xke.type = ntohl(type);
		xke.state = ntohl(state);
		xke.keycode = ntohl(key);

		ev.type = xke.type;
		ev.xkey = xke;

		XSendEvent(dpy, InputFocus, true, KeyPressMask|KeyReleaseMask, &ev);
		XFlush(dpy);

		printf("Done.\n");
	}

}
