#include "sender.hh"

#include <cstring>
#include <cstdlib>
#include <cstdio>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <X11/Xlib.h>

void
sender_waitrecievers(uint16_t port, int clientcount, int * clientsocks) {

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	int enable = 1;

	if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
		perror("setsockopt()");
	}

	struct sockaddr_in inaddr;
	inaddr.sin_family = AF_INET;
	inaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	inaddr.sin_port = htons(port);

	if(bind(sockfd, (sockaddr *) &inaddr, sizeof(inaddr)) == -1) {
		perror("bind()");
		exit(0);
	}

	if(listen(sockfd, 5) == -1) {
		perror("bind()");
		exit(0);
	}

	for(int i = 0; i < clientcount; ++i) {

		struct sockaddr_in remoteaddr;
		socklen_t remoteaddrlen;

		int clientsockfd = accept(sockfd, (struct sockaddr *) &remoteaddr, &remoteaddrlen);
		clientsocks[i] = clientsockfd;

		char addrbuf[3*4+4+2];
		inet_ntop(remoteaddr.sin_family, (void *) &(remoteaddr.sin_addr), addrbuf, 3*4+4+2);

		printf("Receiver %s connected.\n", addrbuf);
	}
}

void
sender_send(XKeyEvent * event, int * clientsocks, size_t clientcount) {

	size_t bufsize = 3*4 + 1; // 3 Integers + 0db
	char buf[bufsize];
	buf[bufsize-1] = 0xdb;

	event->type = htonl(event->type);
	event->state = htonl(event->state);
	event->keycode = htonl(event->keycode);

	memcpy(&buf[0], &event->type, sizeof(unsigned int));
	memcpy(&buf[4], &event->state, sizeof(unsigned int));
	memcpy(&buf[8], &event->keycode, sizeof(unsigned int));

	for(int i = 0; i < clientcount; ++i) {
		int clientsockfd = clientsocks[i];
		send(clientsockfd, buf, bufsize, 0);
	}

}

void
sender_run(int clientcount, uint16_t port) {

	/* Prepare connection structures */
	int clientsocks[clientcount];

	/* Wait for connection */
	printf("Waiting for receivers to connect...\n");
	sender_waitrecievers(port, clientcount, clientsocks);
	printf("All receivers connected.\n");

	/* Open Connection to X Server */
	Display * dpy = XOpenDisplay(NULL);
	Window window = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 100, 100, 200, 100, 0, 0, BlackPixel(dpy, DefaultScreen(dpy)));

	XClearWindow(dpy, window);
	XMapRaised(dpy, window);
	XSelectInput(dpy, window, KeyPressMask | KeyReleaseMask);

	XEvent ev;
	while(true) {
		XNextEvent(dpy, &ev);

		switch(ev.type) {

			case KeyPress:
			case KeyRelease: {
				sender_send(&ev.xkey, clientsocks, clientcount);
				break;
			}
		}
	}

	for(int i = 0; i < clientcount; ++i) {
		close(clientsocks[i]);
	}
}
