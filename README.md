# keyshare
Enables the User to use a single Keyboard on multiple Devices remotely.

# Dependencies

* SDL2
* X11

# Compiling

```bash
make keyshare
```

# Installation

```bash
make keyshare && make install
```

# Uninstallation

```bash
make remove
```

# Usage

To make it work, run the Server on the Machine that has the Keyboard attached to it.
You then need the IP(v4) Address of this Machine in your local network. ***Let's assume***
this Address is **192.168.0.145**. You have to feed this Address to the Client, which runs
on the Machine that wants to receive the Keyboard Inputs, with the -i Option.
See below for examples.

If you need to run the Application on another Port, you can use the -p Option. Make
sure, you use this on both Ends.

Use the following Command to display the "help" Message:
```bash
keyshare help
```

To Start a Server use:
```bash
keyshare server
```

To start a Client use:
```bash
keyshare client -i 192.168.0.145
```
